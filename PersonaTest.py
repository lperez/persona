from Persona import Persona
from Persona import Paciente
from Persona import Medico
import unittest

class PersonaTestCase(unittest.TestCase):
    def test_paciente(self):
        Paciente01 = Paciente('Jose', 'Santos García', '04/10/1988', '70982713R', 'Cáncer' )
        historial = Paciente01.VerHistorialMedico()
        self.assertEqual(historial, 'Cáncer')

    def test_medico(self):
        Medico01 = Medico('Antonio', ' Sánchez Rodríguez','03/04/1992', '54329751J', 'Cardiología', '4 de abril')
        Agenda = Medico01.consultar_agenda()
        self.assertEqual(Agenda,('Cardiología', '4 de abril'))

unittest.main()